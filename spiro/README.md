This program makes spirographs. In this project, you’ll learn how to draw spiros on your computer. You’ll also learn how to do the following:
1. Create graphics with the ``turtle`` module.
2. Use parametric equations.
3. Use mathematical equations to generate curves.
4. Draw a curve using lines.
5. Use a timer to animate graphics.
6. Save graphics to image files.

A word of caution about this project: I’ve chosen to use the turtle module for this project mainly for illustrative purposes and because it’s fun, but turtle is slow and not ideal for creating graphics when performance is critical. (What do you expect from turtles?) If you want to draw something fast, there are better ways to do so, and you’ll explore some of these options in upcoming projects.

To run this program please enter the following code in the console.
	``python spiro.py``
