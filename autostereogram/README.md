This is another program which makes an autostereogram of a depth map. Auto­stereo­grams usually consist of repeating patterns that resolve into three dimensions on closer inspection. If you can’t see any sort of image, don’t worry; it took me a while and a bit of experimentation before I could.

In this project, you’ll use Python to create an autostereogram. Here are some of the concepts covered in this project:
1. Linear spacing and depth perception
2. Depth maps
3. Creating and editing images using Pillow
4. Drawing into images using Pillow

The autostereograms you’ll generate in this project are designed for “wall-eyed” viewing. The best way to see them is to focus your eyes on a point behind the image (such as a wall). Almost magically, once you perceive something in the patterns, your eyes should automatically bring it into focus, and when the three-dimensional image “locks in,” you will have a hard time shaking it off. (If you’re still having trouble viewing the image, see Gene Levin’s article “How to View Stereograms and Viewing Practice” for help.)

An autostereogram works by changing the linear spacing between patterns in an image, thereby creating the illusion of depth. When you look at repeating patterns in an autostereogram, your brain can interpret the spacing as depth information, especially if there are multiple patterns with different spacing.

In this project, you’ll use ``Pillow`` to read in images, access their underlying data, and create and modify images.

To display the autostereogram in the computer you need to write the following code in your console:
	``python3 autos.py --depth <depth map>``
