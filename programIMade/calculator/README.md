This program I made is a small but very useful program. I first tried it by giving options but then I had a problem in get the trig functions, so I made this program which has almost like all the functions of mathematics like- log, sin, cos, asin, etc. I made it for my elder sister Aarotrika Pramanik because she wanted to be a mathematician.

I used:
	1. The ``numpy`` module
	2. The ``eval`` function

The language is Python only but in the input box you don't need to write like ``numpy.sin`` instead you just write ``sin`` as I imported everything in ``numpy``.

To run this program you should write the following script:
	``python3 calculator.py``

