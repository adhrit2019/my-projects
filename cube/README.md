In this program we shall be able to visualize a rubik's cube using ``matplotlib``. But I didn't copy this program from the book, instead I copied it from someone's github account. This program's own default cube is the 3 by 3 cube. I mean that when we run the following program it shows a 3 by 3 cube. If you don't want a 3 by 3 cube then write the following code on your console:
	``python3 cube_interactive.py 4``
This will make a 4 by 4 cube.
