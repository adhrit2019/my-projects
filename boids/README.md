This program simulates a flock of birds. We can also control the flock. But this program has got the older version of ``matplolib`` while I have the latest version. If you have the older version then you can control the flock.

The three core rules of the Boids simulation are as follows:
	**Separation** Keep a minimum distance between the boids.
	**Alignment** Point each boid in the average direction of movement of its local flockmates.
	**Cohesion** Move each boid toward the center of mass of its local flockmates.

Boids simulations can add other rules, too, such as ones to avoid obstacles or scatter the flock when it’s disturbed, as you’ll learn in the following sections. This version of Boids implements these core rules for every step in the simulation:
	1. For all boids in the flock, do the following:
		- Apply the three core rules.
		- Apply any additional rules.
		- Apply all boundary conditions.
	2. Update the positions and velocities of the boids.
	3. Plot the new positions and velocities.
As you will see, these simple rules create a flock with evolving, complex behavior.

These are the Python tools you’ll be using in this simulation:
	1. ``numpy`` arrays to store the positions and velocities of the boids
	2. The ``matplotlib`` library to animate the boids
	3. ``argparse`` to process command line options
	4. The ``scipy.spatial.distance`` module, which has some really neat methods for calculating distances between points

To run this program in the console, write the following code in the console:
	``python2 boids.py``
